====THAUMCRAFT API
	main.utils.compat.ThaumcraftHelper
		L201-233	addItemsToBackpack (not called at the moment anyway)
		L547	Aspect.SEED
		L554	Aspect.STONE
		L616	Aspect.ICE

main.utils.TickHandlerVersion
	Disabled.

main.utils.VersionInfo
	Disabled Logger modLogger, temporarily replaced with direct access to FMLLog (FML log switched to log4j).
	Creation of TickHandlerVersion instance disabled (have not reprogrammed to work with the new tick handler setup).

main.Config
	setupBlocks()
		Block registration & ItemBlock creation. (planksWood, slabWoodFull & slabWoodHalf) - ItemBlocks now registered with the block in GameRegistry.registerBlock, but I cannot seem to find calls to registerBlock for the above blocks.
		BuildCraft Facade IMC

world.feature.FeatureHive
	Replaced minHeight with rootHeight.
	Replaced maxHeight with heightVariation.
	I am not at all certain if these are accurate.